import Header from './components/Header'
import Items from './components/Items'
import TotalPrice from './components/TotalPrice'
import AddItem from './components/AddItem'
import { useState } from 'react'

function App() {
    const [showAddItem, setShowAddItem] = useState(false)
    const [items, setItems] = useState([
        {
            id: 1,
            title: 'Ice cream',
            quantity: 5,
            price: 2,
            cost: 10,
        },
        {
            id: 2,
            title: 'Ice cream',
            quantity: 5,
            price: 2,
            cost: 10,
        },
        {
            id: 3,
            title: 'Ice cream',
            quantity: 5,
            price: 4,
            cost: 20,
        },
    ])

    //Total Price--------------------------------------
    var totalPrice = 0
    items.map((item) => (totalPrice += item.cost))

    //Reset Items--------------------------------------
    const resetItems = () => {
        setItems(items.map((item) => ({ ...item, cost: 0, quantity: 0 })))
    }

    //Cut Item-----------------------------------------
    const cutItem = (id) => {
        setItems(
            items.map((item) =>
                item.id === id
                    ? {
                          ...item,
                          quantity: item.quantity - 1,
                          cost: item.cost - item.price,
                      }
                    : item
            )
        )
    }

    //Add Item-------------------------------------------
    const addItem = (id) => {
        setItems(
            items.map((item) =>
                item.id === id
                    ? {
                          ...item,
                          quantity: item.quantity + 1,
                          cost: item.cost + item.price,
                      }
                    : item
            )
        )
    }

    //remove Item-----------------------------------------
    const removeItem = (id) => {
        setItems(items.filter((item) => item.id !== id))
    }

    //Show AddItem Form ----------------------------------
    const showAdd = () => {
        setShowAddItem(!showAddItem)
    }

    //Add New Item----------------------------------------
    const addNewItem = (item) => {
        let id = items.length > 0 ? items[items.length - 1].id + 1 : 1
        item = { id: id, ...item }
        setItems([...items, item])
    }

    return (
        <div className="container">
            <Header
                resetItems={resetItems}
                showAdd={showAdd}
                showAddItem={showAddItem}
            />
            {showAddItem && <AddItem addNewItem={addNewItem} />}
            {items.length > 0 ? (
                <Items
                    items={items}
                    cutItem={cutItem}
                    addItem={addItem}
                    removeItem={removeItem}
                />
            ) : (
                'There are no items.'
            )}
            <TotalPrice totalPrice={totalPrice} />
        </div>
    )
}

export default App
