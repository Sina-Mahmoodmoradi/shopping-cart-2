import { useState } from 'react'
const AddItem = ({ addNewItem }) => {
    const [title, setTitle] = useState('')
    const [price, setPrice] = useState(0)
    const [quantity, setQuantity] = useState(0)

    const onSubmit = (e) => {
        e.preventDefault()
        if (!title) {
            alert("Enter Item's Title.")
            return
        }
        addNewItem({ title, quantity, price, cost: quantity * price })
        setTitle('')
        setPrice(0)
        setQuantity(0)
    }
    return (
        <div className="addItem">
            <form className="form" onSubmit={onSubmit}>
                <div className="form-control">
                    <label>Title:</label>
                    <input
                        type="text"
                        name="title"
                        placeholder="New Item..."
                        value={title}
                        onChange={(e) => setTitle(e.target.value)}
                    />
                </div>
                <div className="form-control">
                    <label>Price:</label>
                    <input
                        type="number"
                        name="title"
                        value={price}
                        onChange={(e) => setPrice(Number(e.target.value))}
                    />
                </div>
                <div className="form-control">
                    <label>Quantity:</label>
                    <input
                        type="number"
                        name="title"
                        value={quantity}
                        onChange={(e) => setQuantity(Number(e.target.value))}
                    />
                </div>
                <input type="submit" value="Add Item" className="submitBtn" />
            </form>
        </div>
    )
}

export default AddItem
