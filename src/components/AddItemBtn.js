const AddItemBtn = ({ showAdd, showAddItem }) => {
    return (
        <button
            className="btn"
            style={
                showAddItem
                    ? { backgroundColor: 'grey', color: 'white' }
                    : { backgroundColor: 'green', color: 'white' }
            }
            onClick={showAdd}
        >
            {showAddItem ? 'Close' : 'Add Item'}
        </button>
    )
}

export default AddItemBtn
