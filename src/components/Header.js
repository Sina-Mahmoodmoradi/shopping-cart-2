import AddItemBtn from './AddItemBtn'
import ResetItemsBtn from './ResetItemsBtn'

const Header = ({ resetItems, showAdd, showAddItem }) => {
    return (
        <header className="header">
            <AddItemBtn showAdd={showAdd} showAddItem={showAddItem} />
            <ResetItemsBtn resetItems={resetItems} />
            <h1>My Cart</h1>
        </header>
    )
}

export default Header
