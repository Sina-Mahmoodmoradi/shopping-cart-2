const Item = ({ item, addItem, cutItem, removeItem }) => {
    return (
        <div className="item">
            <h3>{item.title}</h3>
            <button
                className="btn"
                style={{ backgroundColor: 'black', color: 'white' }}
                onClick={() => removeItem(item.id)}
            >
                X
            </button>
            <button
                className="btn"
                style={{ backgroundColor: 'red', color: 'white' }}
                onClick={() => addItem(item.id)}
            >
                Add
            </button>
            <button
                className="btn"
                style={{ backgroundColor: 'green', color: 'white' }}
                onClick={() => cutItem(item.id)}
            >
                Cut
            </button>
            <div>Quantity: {item.quantity}</div>
            <div>Price: ${item.price}</div>
            <div>Cost: ${item.cost}</div>
        </div>
    )
}

export default Item
