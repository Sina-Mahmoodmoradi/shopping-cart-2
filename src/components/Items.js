import Item from './Item'

const items = ({ items, addItem, cutItem, removeItem }) => {
    return (
        <div className="Items">
            {items.map((item) => (
                <Item
                    key={item.id}
                    item={item}
                    addItem={addItem}
                    cutItem={cutItem}
                    removeItem={removeItem}
                />
            ))}
        </div>
    )
}

export default items
