const ResetItemsBtn = ({ resetItems }) => {
    return (
        <button
            className="btn"
            style={{ backgroundColor: 'red', color: 'white' }}
            onClick={resetItems}
        >
            Reset Items
        </button>
    )
}

export default ResetItemsBtn
