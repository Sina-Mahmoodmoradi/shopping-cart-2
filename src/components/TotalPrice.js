const TotalPrice = ({ totalPrice }) => {
    return <div className="totalPrice">Total Price: ${totalPrice}</div>
}

export default TotalPrice
